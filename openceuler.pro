TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += OpenCLProcs.cpp \
    TickTackle.cpp
SOURCES += main.cpp
HEADERS += OpenCLProcs.h \
    TickTackle.h
HEADERS += logger.h

INCLUDEPATH += /usr/include/nvidia-375

LIBS += -lOpenCL

DISTFILES += test.cl

LIBS += -lopencv_core
LIBS += -lopencv_imgproc
LIBS += -lopencv_highgui
LIBS += -lopencv_ocl
