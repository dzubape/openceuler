#include "logger.h"
LOGGER_CREATE();

#include "OpenCLProcs.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ocl/ocl.hpp>

namespace yokcl { using namespace OpenCLProc; }

int main(int, char**) {

#if 0
    cv::ocl::oclMat oclSrc(cv::imread("test.png"));

    cv::Mat src;
    oclSrc.download(src);

    cv::imshow("ocl", src);

    cv::waitKey();

    return 0;
#else
    cv::Mat src = cv::imread("test.png");
    cv::Mat dst;

    for(int i=1; true; ++i) {

        bool ok = yokcl::blur(src, dst, i, yokcl::BLUR_SPHERIC);

        std::cout << std::endl << (ok ? "ok" : "fucked!");

        cv::imshow("dst", dst);

        unsigned char c = (cv::waitKey() & 255);
        if(c == 27) {

            break;
        }
    }
#endif
    return 0;
}
