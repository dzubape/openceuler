#ifndef TICKTACKLE_H
#define TICKTACKLE_H

#include <chrono>

class TickTackle {
public:
    TickTackle();

    /// @brief Use to set start mark of period measuring
    void tick();

    /// @brief Use to fix current time (in microseconds) from the begining
    size_t tack();

    /// @brief Use to get current fixed period duration (in microseconds)
    size_t duration() const;

private:
    std::chrono::steady_clock::time_point m_start, m_end;
};

#endif // TICKTACKLE_H
