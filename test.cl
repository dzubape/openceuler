__kernel void blur(
    __global const uchar *src, // линейный массив входных данных изображения
    const int cols, // ширина изображения
    const int rows, // высота изображения
    const int channels, // количество каналов исходного изображения
    __global uchar *dst, // линейный массив выходных данных изображения
    const int radius, // радиус размытия
    __global const float *core // ядро коэффициентов (квадратная матрица со стороной radius * 2 - 1
) {
    int pxlIdx = get_global_id(0);

    int pxlCount = rows * cols;

    if(pxlIdx >= pxlCount) {

        return;
    }

    int pxlX = pxlIdx % cols;
    int pxlY = pxlIdx / rows;

    int center = radius - 1;
    int coreSide = radius * 2 - 1;
    float square = 0.0;
    int accum[3];
    for(int i=0; i<channels; ++i) {

        accum[i] = 0;
    }

    for(int j=-center; j<=center; ++j) {

        int y = pxlY + j;

        if(y < 0) {

            y = 0;
        }
        else if(y >= rows) {

            y = rows - 1;
        }

        for(int i=-center; i<=center; ++i) {

            int x = pxlX + i;

            if(x < 0) {

                x = 0;
            }
            else if(x >= cols) {

                x = cols - 1;
            }

            __global const uchar *srcPxl = src + (y * cols + x) * 3;

            float k = core[(j + center) * coreSide + i + center];

            for(char c=0; c<channels; ++c) {

                accum[c] += srcPxl[c] * k;
            }

            square += k;
        }
    }


    for(char c=0; c<channels; ++c) {

        dst[pxlIdx*channels + c] = accum[c] / square;
    }
}
