#include "logger.h"
LOGGER_EXTERN();

#include "OpenCLProcs.h"

#include <CL/cl.h>
#include <iostream>
#include <vector>

#include "TickTackle.h"

#include <stdio.h>
#include <memory.h>

#include <opencv2/imgproc/imgproc.hpp>

namespace OpenCLProc {

class OpenCLManager {
private:
    static OpenCLManager *m_instancePtr;

    cl_platform_id clPlatformId;
    cl_context m_clContext;

    bool m_isValid;

private:
    OpenCLManager() {

        m_isValid = init();
    }

    bool init() {

        const cl_uint clPlatformMaxCount = 3;
        cl_uint clPlatformCount = 0;
        cl_platform_id clPlatformIds[clPlatformMaxCount];
        cl_int clError;

        clError = clGetPlatformIDs(clPlatformMaxCount, clPlatformIds, &clPlatformCount);

        if(clError != CL_SUCCESS) {

            LOG() << "Ошибка при получении списка платформ!";

            return false;
        }
        else {

            LOG() << "Получен список платформ.";
        }

        if(!clPlatformCount) {

            LOG() << "Не найдено ни одной OpenCL-платформы";
            return false;
        }

        LOG() << "Количество платформ: " << clPlatformCount;

        clPlatformId = clPlatformIds[0];


        // Устройства, работающие на данной платформе

        const cl_uint clDeviceMaxCount = 16;
        cl_uint clDeviceCount = 0;
        cl_device_id clDeviceIds[clDeviceMaxCount];

        clError = clGetDeviceIDs(clPlatformId, CL_DEVICE_TYPE_ALL, clDeviceMaxCount, clDeviceIds, &clDeviceCount);

        if(clError != CL_SUCCESS) {

            LOG() << "Выбранную платформу не поддерживает ни одно устройство в системе";
            return false;
        }

        m_clContext = clCreateContext(NULL, clDeviceCount, clDeviceIds, NULL, NULL, &clError);

        if(clError != CL_SUCCESS) {

            LOG() << "При попытке создать контекст для платформы " << clPlatformId << " возникла ошибка: " << clError;
        }

        LOG() << "Создан контекст " << m_clContext;

        return true;
    }

    ~OpenCLManager() {

        clReleaseContext(m_clContext);

        m_instancePtr = nullptr;
    }

public:
    static OpenCLManager* instance() {

        if(!m_instancePtr) {

            m_instancePtr = new OpenCLManager();
        }

        return m_instancePtr;
    }

    static void release() {

        delete m_instancePtr;
    }

    bool isValid() const {

        return m_isValid;
    }

    cl_context getContext() const {

        return m_clContext;
    }

    cl_command_queue createQueue() {

        // Устройства, работающие на данной платформе

        const cl_uint clDeviceMaxCount = 16;
        cl_uint clDeviceCount = 0;
        cl_device_id clDeviceIds[clDeviceMaxCount];

        cl_int clError = clGetDeviceIDs(clPlatformId, CL_DEVICE_TYPE_ALL, clDeviceMaxCount, clDeviceIds, &clDeviceCount);

        if(clError != CL_SUCCESS) {

            LOG() << "Выбранную платформу не поддерживает ни одно устройство в системе";
            return nullptr;
        }

        if(!clDeviceCount) {

            return nullptr;
        }

        cl_device_id clDeviceId = clDeviceIds[0];

        cl_command_queue clCommandQueue = clCreateCommandQueue(m_clContext, clDeviceId, 0, &clError);

        if(clError != CL_SUCCESS) {

            LOG() << "Ошибка при попытке создания очереди для выбранного устройства: " << clError;
            return nullptr;
        }

        return clCommandQueue;
    }

    static size_t readProgSrcFile(const char* srcFilePath, char *src) {

        using namespace std;

        FILE *fd = fopen(srcFilePath, "r");
        if(!fd) {

            return 0L;
        }

        fseek(fd, 0L, SEEK_END);
        size_t srcFileSize = ftell(fd);

        if(!srcFileSize) {

            return 0L;
        }

        src = (char*)malloc(sizeof(char) * (srcFileSize + 1));

        fseek(fd, 0L, SEEK_SET);
        fread(src, sizeof(char), srcFileSize, fd);
        src[srcFileSize] = '\0';
        fclose(fd);

        return srcFileSize;
    }

    static cl_program createProgFromSrc(const char* srcFilePath) {

        using namespace std;

        FILE *fd = fopen(srcFilePath, "r");
        if(!fd) {

            return nullptr;
        }

        fseek(fd, 0L, SEEK_END);
        size_t srcFileSize = ftell(fd);

        if(!srcFileSize) {

            return nullptr;
        }

        char *progSrc;

        progSrc = (char*)malloc(sizeof(char) * (srcFileSize + 1));

        fseek(fd, 0L, SEEK_SET);
        fread(progSrc, sizeof(char), srcFileSize, fd);
        progSrc[srcFileSize] = '\0';
        fclose(fd);

        if(!srcFileSize) {

            LOG() << "При попытке чтения исходного кода kernel из " << srcFilePath << " произошла ошибка";
            return nullptr;
        }

#if 0
        LOG() << "Текст kernel'а:" << std::endl << progSrc;
#endif

        cl_int clError;

        cl_program clProgram = clCreateProgramWithSource(instance()->getContext(), 1, (const char**)&progSrc, &srcFileSize, &clError);
        if(clError != CL_SUCCESS) {

            LOG() << "При попытке создания программного объекта из исходного кода kernel произошла ошибка: " << clError;
            return nullptr;
        }

        LOG() << "Создан программный объект OpenCL: " << clProgram;

        clError = clBuildProgram(clProgram, 0, NULL, NULL, NULL, NULL);
        if(clError != CL_SUCCESS) {

            LOG() << "При компиляции программного объекта произошла ошибка: " << clError;
            return nullptr;
        }

        LOG() << "Указанный программный объект OpenCL скомпилирован";

        free(progSrc);

        return clProgram;
    }

}; // class OpenCLManager

OpenCLManager* OpenCLManager::m_instancePtr = nullptr;


std::string readProgSrcFile(const char* srcFilePath) {

    char *src = nullptr;
    size_t length = OpenCLManager::readProgSrcFile(srcFilePath, src);
    std::string strSrc(src);
    delete src;

    return strSrc;
}


void test() {

    cl_int clError;

    OpenCLManager *ocl = OpenCLManager::instance();

    if(!ocl->isValid()) {

        return;
    }

    cl_context clContext = ocl->getContext();

    cl_command_queue clCommandQueue = ocl->createQueue();

    if(!clCommandQueue) {

        LOG() << "При создании очереди произошла ошибка";

        return;
    }

    cl_program clProgram = OpenCLManager::createProgFromSrc("test.cl");

    // Формируем данные
    const size_t vectorSize = 1024*1024*256;

    cl_mem clBufferA = clCreateBuffer(clContext, CL_MEM_READ_ONLY, sizeof(cl_int) * vectorSize, NULL, &clError);
    if(clError != CL_SUCCESS) {

        LOG() << "При создании буффера clSrcBufferA произошла ошибка: " << clError;
        return;
    }

    cl_mem clBufferB = clCreateBuffer(clContext, CL_MEM_READ_ONLY, sizeof(cl_int) * vectorSize, NULL, &clError);
    if(clError != CL_SUCCESS) {

        LOG() << "При создании буффера clSrcBufferB произошла ошибка: " << clError;
        return;
    }

    cl_mem clBufferC = clCreateBuffer(clContext, CL_MEM_WRITE_ONLY, sizeof(cl_int) * vectorSize, NULL, &clError);
    if(clError != CL_SUCCESS) {

        LOG() << "При создании буффера clDstBufferC произошла ошибка: " << clError;
        return;
    }

    cl_kernel clKernel = clCreateKernel(clProgram, "vectorSum", &clError);
    if(clError != CL_SUCCESS) {

        LOG() << "При создании kernel'а произошла ошибка: " << clError;
        return;
    }

    clSetKernelArg(clKernel, 0, sizeof(cl_mem), &clBufferA);
    clSetKernelArg(clKernel, 1, sizeof(cl_mem), &clBufferB);
    clSetKernelArg(clKernel, 2, sizeof(cl_mem), &clBufferC);
    clSetKernelArg(clKernel, 3, sizeof(cl_int), &vectorSize);

    cl_int *srcA = (cl_int*)malloc(sizeof(cl_int) * vectorSize);
    cl_int *srcB = (cl_int*)malloc(sizeof(cl_int) * vectorSize);
    cl_int *dstC = (cl_int*)malloc(sizeof(cl_int) * vectorSize);

    for(size_t i=0; i<vectorSize; ++i) {

        srcA[i] = 3;
        srcB[i] = 4;
    }

    clEnqueueWriteBuffer(clCommandQueue, clBufferA, CL_FALSE, 0, vectorSize * sizeof(cl_int), srcA, 0, NULL, NULL);
    clEnqueueWriteBuffer(clCommandQueue, clBufferB, CL_FALSE, 0, vectorSize * sizeof(cl_int), srcB, 0, NULL, NULL);

    clEnqueueNDRangeKernel(clCommandQueue, clKernel, 1, NULL, &vectorSize, NULL, 0, NULL, NULL);

    clEnqueueReadBuffer(clCommandQueue, clBufferC, CL_TRUE, 0, sizeof(cl_int) * vectorSize, dstC, 0, NULL, NULL);

    for(int i=0; i<10; ++i) {

        LOG() << i << ": " << dstC[i];
    }

    // Освобождаем ресурсы
    free(srcA);
    free(srcB);
    free(dstC);

    clReleaseMemObject(clBufferA);
    clReleaseMemObject(clBufferB);
    clReleaseMemObject(clBufferC);

    clReleaseKernel(clKernel);
    clReleaseCommandQueue(clCommandQueue);
    clReleaseProgram(clProgram);

    return;
}


#define __kernel
#define __global

__kernel void blurKernel(
    __global const uchar *src, // линейный массив входных данных изображения
    const int cols, // ширина изображения
    const int rows, // высота изображения
    const int channels, // количество каналов исходного изображения
    __global uchar *dst, // линейный массив выходных данных изображения
    int radius, // радиус размытия
    __global const float *core // ядро коэффициентов (квадратная матрица со стороной radius * 2 - 1
#if 1
    , int pxlIdx
) {
#else
) {
    int pxlIdx = get_global_id(0);
#endif
    int pxlCount = rows * cols;

    if(pxlIdx >= pxlCount) {

        return;
    }

    int pxlX = pxlIdx % cols;
    int pxlY = pxlIdx / rows;

    int center = radius - 1;
    int coreSide = radius * 2 - 1;
    float square = 0.0;
    int accum[channels];
    //LOG() << "channels: " << channels;
#if 1
    for(int i=0; i<channels; ++i) {

        accum[i] = 0;
    }
#endif

    for(int j=-center; j<=center; ++j) {

        int y = pxlY + j;

        if(y < 0) {

            y = 0;
        }
        else if(y >= rows) {

            y = rows - 1;
        }

        for(int i=-center; i<=center; ++i) {

            int x = pxlX + i;

            if(x < 0) {

                x = 0;
            }
            else if(x >= cols) {

                x = cols - 1;
            }

            __global const uchar *srcPxl = src + (y * cols + x) * 3;

            float k = core[(j + center) * coreSide + i + center];

#if 1
            for(uchar c=0; c<channels; ++c) {

                accum[c] += srcPxl[c] * k;
            }
#else
            uchar c = 0;
            accum[c++] += srcPxl[c] * k;
            accum[c++] += srcPxl[c] * k;
            accum[c++] += srcPxl[c] * k;
#endif

            square += k;
        }
    }
#if 1
    for(uchar c=0; c<channels; ++c) {

        dst[pxlIdx*channels + c] = accum[c] / square;
    }
#else
    uchar c = 0;
    dst[pxlIdx*channels + c++] = accum[c] / square;
    dst[pxlIdx*channels + c++] = accum[c] / square;
    dst[pxlIdx*channels + c++] = accum[c] / square;
#endif
}

#undef __kernel
#undef __global

bool blur(const cv::Mat src, cv::Mat &dst, int radius, int type) {

    if(radius <= 0) {

        dst = cv::Mat();

        return false;
    }

    if(radius == 1) {

        src.copyTo(dst);

        return true;
    }

    TickTackle tt;

    cl_int clError;

    OpenCLManager *ocl = OpenCLManager::instance();

    if(!ocl->isValid()) {

        return false;
    }

    cl_context clContext = ocl->getContext();

    cl_command_queue clCommandQueue = ocl->createQueue();

    if(!clCommandQueue) {

        LOG() << "При создании очереди произошла ошибка";

        return false;
    }

    cl_program clProgram = OpenCLManager::createProgFromSrc("test.cl");

    // Формируем данные
    int channels = src.channels();
    //LOG() << "channels: " << channels;
    size_t pixelCount = src.rows * src.cols;

    //LOG() << "pixelCount: " << src.rows << " * " << src.cols << " = " << pixelCount;

    tt.tick();
    cl_mem clMemSrc = clCreateBuffer(clContext, CL_MEM_READ_ONLY, sizeof(cl_uchar) * channels * pixelCount, NULL, &clError);
    size_t durClCreateBuffer = tt.tack();
    LOG() << "clMemSrc creation time: " << durClCreateBuffer;

    if(clError != CL_SUCCESS) {

        LOG() << "При создании буффера clMemSrc произошла ошибка: " << clError;
        return false;
    }

    cl_mem clMemDst = clCreateBuffer(clContext, CL_MEM_WRITE_ONLY, sizeof(cl_uchar) * channels * pixelCount, NULL, &clError);
    if(clError != CL_SUCCESS) {

        LOG() << "При создании буффера clMemDst произошла ошибка: " << clError;
        return false;
    }

    // Ядро размытия
    const int coreDiam = radius * 2 - 1;
    const int coreShell = radius - 1;
    const int radius2 = radius * radius;
    cl_float *core = new cl_float[coreDiam * coreDiam];

    auto printCore = [&] {

        printf("\n");

        for(int j=0; j<coreDiam; ++j) {

            for(int i=0; i<coreDiam; ++i) {

                printf("%.3f\t", core[j * coreDiam + i]);
            }

            printf("\n");
        }
    };

    cl_float (*node)(int x, int y, int radius);

    switch(type) {
    case BLUR_SPHERIC:

        node = [](int x, int y, int radius)->cl_float{

            int x2 = x * x;
            int y2 = y * y;
            int radius2 = radius * radius;
            int h2 = radius2 - (x2 + y2);

            return h2 > 0 ? sqrt(h2) / radius : 0.f;
        };
        break;

    case BLUR_CONE:

        node = [](int x, int y, int radius)->cl_float{

            int x2 = x * x;
            int y2 = y * y;

            return x2 + y2 > 0 ? radius / sqrt(x2 + y2) : 0.f;
        };
        break;

    case BLUR_WHITISH:

        node = [](int x, int y, int radius)->cl_float{

            int x2 = x * x;
            int y2 = y * y;
            float radius2 = radius * radius;

            return x2 + y2 > 0 ? radius2 / (x2 + y2) : radius2;
        };
    }

    for(int j=0; j<radius; ++j) {

        cl_float *row = core + j * coreDiam;
        cl_float *mirrow = core + (coreDiam - 1 - j) * coreDiam;

        int y = abs(j - coreShell);

        for(int i=0; i<radius; ++i) {

            int x = abs(i - coreShell);

            int mirri = coreDiam - 1 - i;

            if(j <= i) {

                row[i] = row[mirri] = mirrow[i] = mirrow[mirri] = node(x, y, radius);
            }
            else {

                row[i] = row[mirri] = mirrow[i] = mirrow[mirri] = (core + i * coreDiam)[j];
            }
        }
    }

#if 0
    printCore();
#endif

    cl_mem clMemCore = clCreateBuffer(clContext, CL_MEM_READ_ONLY, sizeof(cl_float) * coreDiam * coreDiam, NULL, &clError);
    if(clError != CL_SUCCESS) {

        LOG() << "При создании буффера clMemDst произошла ошибка: " << clError;
        return false;
    }

    cl_kernel clKernel = clCreateKernel(clProgram, "blur", &clError);
    if(clError != CL_SUCCESS) {

        LOG() << "При создании kernel'а произошла ошибка: " << clError;
        return false;
    }

    cl_int clRadius = radius;
    cl_int clChannels = src.channels();
    cl_int clRows = src.rows;
    cl_int clCols = src.cols;

    unsigned char arg = 0;
    clSetKernelArg(clKernel, arg++, sizeof(cl_mem), &clMemSrc);
    clSetKernelArg(clKernel, arg++, sizeof(cl_int), &clCols);
    clSetKernelArg(clKernel, arg++, sizeof(cl_int), &clRows);
    clSetKernelArg(clKernel, arg++, sizeof(cl_int), &clChannels);
    clSetKernelArg(clKernel, arg++, sizeof(cl_mem), &clMemDst);
    clSetKernelArg(clKernel, arg++, sizeof(cl_int), &clRadius);
    clSetKernelArg(clKernel, arg++, sizeof(cl_mem), &clMemCore);

    tt.tick();
    clEnqueueWriteBuffer(clCommandQueue, clMemSrc, CL_FALSE, 0, sizeof(cl_char) * 3 * pixelCount, src.data, 0, NULL, NULL);
    clEnqueueWriteBuffer(clCommandQueue, clMemCore, CL_FALSE, 0, sizeof(cl_float) * coreDiam * coreDiam, core, 0, NULL, NULL);
    size_t gpuWriteTime = tt.tack();

    if(dst.cols != src.cols || dst.rows != src.rows || dst.type() != src.type()) {

        dst = cv::Mat(src.rows, src.cols, src.type());
    }

#if 1
    tt.tick();
    clEnqueueNDRangeKernel(clCommandQueue, clKernel, 1, NULL, &pixelCount, NULL, 0, NULL, NULL);
    size_t gpuExecTime = tt.tack();

    tt.tick();
    clEnqueueReadBuffer(clCommandQueue, clMemDst, CL_TRUE, 0, sizeof(cl_uchar) * 3 * pixelCount, dst.data, 0, NULL, NULL);
    size_t gpuReadTime = tt.tack();
    LOG() << "GPU write duration: \t" << gpuWriteTime << " mks";
    LOG() << "GPU exec duration: \t" << gpuExecTime << " mks";
    LOG() << "GPU read duration: \t" << gpuReadTime << " mks";
    size_t gpuFullTime = gpuWriteTime + gpuExecTime + gpuReadTime;
    LOG() << "GPU write + exec + read duration: \t" << gpuFullTime << " mks";
#endif

#if 0
    tt.tick();
    for(int i=0; i<pixelCount; ++i) {

        blurKernel(src.data, src.cols, src.rows, src.channels(), dst.data, radius, core, i);
    }
    size_t cpuExecTime = tt.tack();
    size_t cpuFullTime = cpuExecTime;
    LOG() << "CPU blur duration: \t" << cpuFullTime << " mks";

#if 1
    float ratioGpu2Cpu = cpuFullTime / gpuFullTime;
    LOG() << "GPU vs. CPU: " << ratioGpu2Cpu << " times faster";
#endif
#endif

    // Освобождаем ресурсы

    delete[] core;

    clReleaseMemObject(clMemSrc);
    clReleaseMemObject(clMemDst);
    clReleaseMemObject(clMemCore);

    clReleaseKernel(clKernel);
    clReleaseCommandQueue(clCommandQueue);
    clReleaseProgram(clProgram);

    return true;
}

bool getContours(const cv::Mat src, cv::Mat &dst, int radius, int type) {

    if(radius <= 0) {

        dst = cv::Mat();

        return false;
    }

    if(radius == 1) {

        src.copyTo(dst);

        return true;
    }

    TickTackle tt;

    cl_int clError;

    OpenCLManager *ocl = OpenCLManager::instance();

    if(!ocl->isValid()) {

        return false;
    }

    cl_context clContext = ocl->getContext();

    cl_command_queue clCommandQueue = ocl->createQueue();

    if(!clCommandQueue) {

        LOG() << "При создании очереди произошла ошибка";

        return false;
    }

    cl_program clProgram = OpenCLManager::createProgFromSrc("test.cl");

    // Формируем данные
    int channels = src.channels();
    //LOG() << "channels: " << channels;
    size_t pixelCount = src.rows * src.cols;

    //LOG() << "pixelCount: " << src.rows << " * " << src.cols << " = " << pixelCount;

    tt.tick();
    cl_mem clMemSrc = clCreateBuffer(clContext, CL_MEM_READ_ONLY, sizeof(cl_uchar) * channels * pixelCount, NULL, &clError);
    size_t durClCreateBuffer = tt.tack();
    LOG() << "clMemSrc creation time: " << durClCreateBuffer;

    if(clError != CL_SUCCESS) {

        LOG() << "При создании буффера clMemSrc произошла ошибка: " << clError;
        return false;
    }

    cl_mem clMemDst = clCreateBuffer(clContext, CL_MEM_WRITE_ONLY, sizeof(cl_uchar) * channels * pixelCount, NULL, &clError);
    if(clError != CL_SUCCESS) {

        LOG() << "При создании буффера clMemDst произошла ошибка: " << clError;
        return false;
    }

    // Ядро размытия
    const int coreDiam = radius * 2 - 1;
    const int coreShell = radius - 1;
    const int radius2 = radius * radius;
    cl_float *core = new cl_float[coreDiam * coreDiam];

    auto printCore = [&] {

        printf("\n");

        for(int j=0; j<coreDiam; ++j) {

            for(int i=0; i<coreDiam; ++i) {

                printf("%.3f\t", core[j * coreDiam + i]);
            }

            printf("\n");
        }
    };

    cl_float (*node)(int x, int y, int radius);

    switch(type) {
    case BLUR_SPHERIC:

        node = [](int x, int y, int radius)->cl_float{

            int x2 = x * x;
            int y2 = y * y;
            int radius2 = radius * radius;
            int h2 = radius2 - (x2 + y2);

            return h2 > 0 ? sqrt(h2) / radius : 0.f;
        };
        break;

    case BLUR_CONE:

        node = [](int x, int y, int radius)->cl_float{

            int x2 = x * x;
            int y2 = y * y;

            return x2 + y2 > 0 ? radius / sqrt(x2 + y2) : 0.f;
        };
        break;

    case BLUR_WHITISH:

        node = [](int x, int y, int radius)->cl_float{

            int x2 = x * x;
            int y2 = y * y;
            float radius2 = radius * radius;

            return x2 + y2 > 0 ? radius2 / (x2 + y2) : radius2;
        };
    }

    for(int j=0; j<radius; ++j) {

        cl_float *row = core + j * coreDiam;
        cl_float *mirrow = core + (coreDiam - 1 - j) * coreDiam;

        int y = abs(j - coreShell);

        for(int i=0; i<radius; ++i) {

            int x = abs(i - coreShell);

            int mirri = coreDiam - 1 - i;

            if(j <= i) {

                row[i] = row[mirri] = mirrow[i] = mirrow[mirri] = node(x, y, radius);
            }
            else {

                row[i] = row[mirri] = mirrow[i] = mirrow[mirri] = (core + i * coreDiam)[j];
            }
        }
    }

#if 0
    printCore();
#endif

    cl_mem clMemCore = clCreateBuffer(clContext, CL_MEM_READ_ONLY, sizeof(cl_float) * coreDiam * coreDiam, NULL, &clError);
    if(clError != CL_SUCCESS) {

        LOG() << "При создании буффера clMemDst произошла ошибка: " << clError;
        return false;
    }

    cl_kernel clKernel = clCreateKernel(clProgram, "blur", &clError);
    if(clError != CL_SUCCESS) {

        LOG() << "При создании kernel'а произошла ошибка: " << clError;
        return false;
    }

    cl_int clRadius = radius;
    cl_int clChannels = src.channels();
    cl_int clRows = src.rows;
    cl_int clCols = src.cols;

    unsigned char arg = 0;
    clSetKernelArg(clKernel, arg++, sizeof(cl_mem), &clMemSrc);
    clSetKernelArg(clKernel, arg++, sizeof(cl_int), &clCols);
    clSetKernelArg(clKernel, arg++, sizeof(cl_int), &clRows);
    clSetKernelArg(clKernel, arg++, sizeof(cl_int), &clChannels);
    clSetKernelArg(clKernel, arg++, sizeof(cl_mem), &clMemDst);
    clSetKernelArg(clKernel, arg++, sizeof(cl_int), &clRadius);
    clSetKernelArg(clKernel, arg++, sizeof(cl_mem), &clMemCore);

    tt.tick();
    clEnqueueWriteBuffer(clCommandQueue, clMemSrc, CL_FALSE, 0, sizeof(cl_char) * 3 * pixelCount, src.data, 0, NULL, NULL);
    clEnqueueWriteBuffer(clCommandQueue, clMemCore, CL_FALSE, 0, sizeof(cl_float) * coreDiam * coreDiam, core, 0, NULL, NULL);
    size_t gpuWriteTime = tt.tack();

    if(dst.cols != src.cols || dst.rows != src.rows || dst.type() != src.type()) {

        dst = cv::Mat(src.rows, src.cols, src.type());
    }

#if 1
    tt.tick();
    clEnqueueNDRangeKernel(clCommandQueue, clKernel, 1, NULL, &pixelCount, NULL, 0, NULL, NULL);
    size_t gpuExecTime = tt.tack();

    tt.tick();
    clEnqueueReadBuffer(clCommandQueue, clMemDst, CL_TRUE, 0, sizeof(cl_uchar) * 3 * pixelCount, dst.data, 0, NULL, NULL);
    size_t gpuReadTime = tt.tack();
    LOG() << "GPU write duration: \t" << gpuWriteTime << " mks";
    LOG() << "GPU exec duration: \t" << gpuExecTime << " mks";
    LOG() << "GPU read duration: \t" << gpuReadTime << " mks";
    size_t gpuFullTime = gpuWriteTime + gpuExecTime + gpuReadTime;
    LOG() << "GPU write + exec + read duration: \t" << gpuFullTime << " mks";
#endif

#if 0
    tt.tick();
    for(int i=0; i<pixelCount; ++i) {

        blurKernel(src.data, src.cols, src.rows, src.channels(), dst.data, radius, core, i);
    }
    size_t cpuExecTime = tt.tack();
    size_t cpuFullTime = cpuExecTime;
    LOG() << "CPU blur duration: \t" << cpuFullTime << " mks";

#if 1
    float ratioGpu2Cpu = cpuFullTime / gpuFullTime;
    LOG() << "GPU vs. CPU: " << ratioGpu2Cpu << " times faster";
#endif
#endif

    // Освобождаем ресурсы

    delete[] core;

    clReleaseMemObject(clMemSrc);
    clReleaseMemObject(clMemDst);
    clReleaseMemObject(clMemCore);

    clReleaseKernel(clKernel);
    clReleaseCommandQueue(clCommandQueue);
    clReleaseProgram(clProgram);

    return true;
}

} // namespace OpenCLProcs
