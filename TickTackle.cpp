#include "TickTackle.h"

#include <chrono>

using namespace std::chrono;

TickTackle::TickTackle()
{
    tick();
}

void TickTackle::tick() {

    m_start = steady_clock::now();
    m_end = m_start;
}

size_t TickTackle::tack() {

    m_end = steady_clock::now();

    return duration();
}

size_t TickTackle::duration() const {

    size_t duration = duration_cast<microseconds>(m_end - m_start).count();

    return duration;
}
