#ifndef OPENCLPROCS_H
#define OPENCLPROCS_H

#include <string>
#include <opencv2/core/core.hpp>

namespace OpenCLProc {

enum : int {
    BLUR_SPHERIC,
    BLUR_CONE,
    BLUR_WHITISH
};

void test();

bool blur(const cv::Mat src, cv::Mat &dst, int radius = 2, int type = BLUR_SPHERIC);

std::string readProgSrcFile(const char *srcFilePath);

} // namespace OpenCLProcs

#endif // #ifndef OPENCLPROCS_H
