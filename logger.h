#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>

class Logger {
public:
    Logger()
        : m_cout(std::cout)
    {}

    ~Logger() {

        m_cout << std::endl;
    }

    std::ostream& operator()() {

        m_cout << std::endl;
        return m_cout;
    }

    std::ostream &m_cout;
};

#define LOGGER_CREATE() Logger LOG
#define LOGGER_EXTERN() extern Logger LOG

#define LOG_OP LOG

#endif // #ifndef LOGGER_H
